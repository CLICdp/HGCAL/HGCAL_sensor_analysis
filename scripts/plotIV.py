###
# This python scripts allows to perform the IV analysis of the sensor listed in nameSensors using HexPlot.
# The format of the input file name MUST be: Hamamatsu_{$inches}in_{$nCells}ch_{$thickness}um_{$typeSensor}_{#nameSensor}_IV.txt
# The function are the following:
# plotIVmaps: plot GEO plot for each V step listed in voltageGEO_list
# plotIVmapsRatio: plot Ratio GEO plot for each V step listed in voltageGEO_list for two files in input
# plotTotalCurrent: plot total IV current (5th column in the input file recorded for PAD#10)
# plotIVallCells: plot IV curves for all PADs from 1 to nCells
# plotIVSensors: plot IV curves for all PADs from 1 to nCells for all sensors listed in nameSensors and their comparison
# plotIVSensorsTotCurr: plot comparison of total IV current (5th column in the input file recorded for PAD#10) for all sensors listed in nameSensors 
###

import os
import math

voltageGEO_list = list()
nameSensors = list()

analyse_IVmaps       = True
analyse_IVallCells   = False
analyse_IVSensors    = False
analyse_IVmapsRatios = True
analyse_TotalIV	     = False

analyse_CERN   	     = True
analyse_HPK    	     = True

fileNameList_CERN = ""
fileNameList_HPK = ""

inputFolder_CERN = "Lab_6inch_data/"
inputFolder_HPK = "Hamamatsu_6inch_data/"

#! Change this part accordingly!!
#Example: 6in 239ch 20XX
nameSensors.extend(["2001", "2002"])
nameBatch = "20XX"
inches = "6"
nCells = "239"
thickness = "120"
typeSensor = "n"

hexPlotFolder = "../"
#hexPlotFolder = "HGCAL_sensor_analysis/"
hexPlot_exe = hexPlotFolder + "bin/HexPlot "
geometry = hexPlotFolder + "/geo/hex_positions_HPK_256ch_6inch_testcap.txt"

estensions = [".pdf",".png"]

if(typeSensor is "pcom" or typeSensor is "pind"):
	voltageGEO_list.extend(["-100", "-200", "-300", "-500", "-800", "-1000"])
else:
	voltageGEO_list.extend(["100", "200", "300", "500", "800", "1000"])

invertValFlag = ""
if(typeSensor is "pcom" or typeSensor is "pind"):
	invertValFlag = "--invertx --invertVal"

# plot IV maps with HexPlot
def plotIVmaps( inputFolder, fileName, outputFolder, z_min, z_max, logFlag, label):
	for voltageGEO in voltageGEO_list:
		print('Plotting IV map for ' + voltageGEO + "V measurement")
		for estension in estensions:
			command_hexplot_geo = hexPlot_exe + " -i " + inputFolder + fileName + ".txt"
			command_hexplot_geo += " -g " + geometry
			command_hexplot_geo += " -o " + outputFolder + fileName + "_" + voltageGEO + "V" + estension
			command_hexplot_geo += " --IV -p GEO --select " + voltageGEO + " --colorpalette 57 "
			command_hexplot_geo += " " + invertValFlag
			command_hexplot_geo += " --addLabel " + label
			if(logFlag):
				command_hexplot_geo += " --useLogVal"
			command_hexplot_geo += " -z " + z_min + ":" + z_max 
			print(command_hexplot_geo)
			os.system(command_hexplot_geo)
   	return;   

# plot IV ratio maps with HexPlot
def plotIVmapsRatio( inputFolder1, fileName1, inputFolder2, fileName2, outputFolder, z_min, z_max, logFlag, label):
	for voltageGEO in voltageGEO_list:
		print('Plotting IV map ratio for ' + voltageGEO + "V measurement")
		for estension in estensions:
			command_hexplot_gratio = hexPlot_exe + " -i " + inputFolder1 + fileName1 + ".txt," + inputFolder2 + fileName2 + ".txt"
			command_hexplot_gratio += " -g " + geometry
			command_hexplot_gratio += " -o " + outputFolder + fileName2 + "_" + voltageGEO + "V_ratio" + estension
			command_hexplot_gratio += " --IV -p GEO --select " + voltageGEO + " --ratio --colorpalette 57 --nd 2"
			command_hexplot_gratio += " " + invertValFlag
			command_hexplot_gratio += " --addLabel " + label
			if(logFlag):
				command_hexplot_gratio += " --useLogVal"
			command_hexplot_gratio += " -z " + z_min + ":" + z_max 
			print(command_hexplot_gratio)
			os.system(command_hexplot_gratio)
   	return;   

# plot total current with HexPlot
def plotTotalCurrent( inputFolder, fileName, outputFolder, z_min, z_max, logFlag, label):
	for estension in estensions:
		command_hexplot_totcurr = hexPlot_exe + " -i " + inputFolder + fileName + ".txt"
		command_hexplot_totcurr += " -o " + outputFolder + fileName + "_totcurr" + estension
		command_hexplot_totcurr += " --IV -p PAD --sc 10 --if SELECTOR:PADNUM:no:no:VAL"
		command_hexplot_totcurr += " " + invertValFlag
		command_hexplot_totcurr += " --addLabel " + label
		if(logFlag):
			command_hexplot_totcurr += " --useLogVal"
		command_hexplot_totcurr += " -z " + z_min + ":" + z_max 
		print(command_hexplot_totcurr)
		os.system(command_hexplot_totcurr)
   	return;   

# plot IV for all channels with HexPlot
def plotIVallCells( inputFolder, fileName, outputFolder, z_min, z_max, logFlag):
	for estension in estensions:
		command_hexplot_all = hexPlot_exe + " -i " + inputFolder + fileName + ".txt"
		command_hexplot_all += " -g " + geometry
		command_hexplot_all += " -o " + outputFolder + fileName + "_selCh" + estension
		command_hexplot_all += " --IV -p PAD --sc 1-" + nCells
		command_hexplot_all += " " + invertValFlag
		# command_hexplot_all += " --addLabel " + label
		if(logFlag):
			command_hexplot_all += " --useLogVal"
		command_hexplot_all += " -z " + z_min + ":" + z_max 
		print(command_hexplot_all)
		os.system(command_hexplot_all)
   	return;   

# plot IV mean and medial for all channels for more than one sensor with HexPlot
def plotIVSensors( fileNameList, outputFolder, z_min, z_max, logFlag):
	for estension in estensions:
		command_hexplot_allSensors = hexPlot_exe + " -i " + fileNameList
		command_hexplot_allSensors += " -g " + geometry
		command_hexplot_allSensors += " --IV -p PAD --sc 1-" + nCells
		command_hexplot_allSensors += " " + invertValFlag
		command_hexplot_allSensors += " --yestoall --noav "
		# command_hexplot_allSensors += " --addLabel " + label
		if(logFlag):
			command_hexplot_allSensors += " -o " + outputFolder + "comparison_log" + estension
			command_hexplot_allSensors += " --useLogVal"
		else:
			command_hexplot_allSensors += " -o " + outputFolder + "comparison" + estension
		command_hexplot_allSensors += " -z " + z_min + ":" + z_max 
		print(command_hexplot_allSensors)
		os.system(command_hexplot_allSensors)
	return;   

def plotIVSensorsTotCurr( fileNameList, outputFolder, z_min, z_max, logFlag):
	for estension in estensions:
		command_hexplot_allTotcurr = hexPlot_exe + " -i " + fileNameList
		command_hexplot_allTotcurr += " --IV -p PAD --sc 10 --if SELECTOR:PADNUM:no:no:VAL --noav"
		command_hexplot_allTotcurr += " " + invertValFlag
		command_hexplot_allTotcurr += " --addLabel \"Total current\""
		if(logFlag):
			command_hexplot_allTotcurr += " -o " + outputFolder + "comparison_totcurr_log" + estension
			command_hexplot_allTotcurr += " --useLogVal"
		else:
			command_hexplot_allTotcurr += " -o " + outputFolder + "comparison_totcurr" + estension
		command_hexplot_allTotcurr += " -z " + z_min + ":" + z_max 
		print(command_hexplot_allTotcurr)
		os.system(command_hexplot_allTotcurr)

   	return;   

def createFolder( directory ):
	try:
		os.stat(directory)
	except:
		os.mkdir(directory)

#IV map for all sensors
for nameSensor in nameSensors:
	#IV map for single sensor

	#Define output folder for CERN measurements
	outputFolder_CERN = "Hamamatsu_" + inches + "in_" + nCells + "ch_" + nameBatch + "_CERN/"
	createFolder(outputFolder_CERN)
	outputFolder_CERN += thickness + "um/"
	createFolder(outputFolder_CERN)
	outputFolder_CERN += nameSensor + "/"
	createFolder(outputFolder_CERN)

	#Define input file for CERN measurements
	fileName_CERN = "Hamamatsu_" + inches + "in_" + nCells + "ch_" + thickness + "um_" + typeSensor + "_" + nameSensor + "_IV"
	label_sensor_CERN = "\"Sensor: " + inches + "in, " + nCells + "ch, " + nameSensor + ", CERN\""
	fileNameList_CERN += inputFolder_CERN + fileName_CERN + ".txt,"

	#Define output folder for HPK measurement
	outputFolder_HPK = "Hamamatsu_" + inches + "in_" + nCells + "ch_" + nameBatch + "_HPK/"
	createFolder(outputFolder_HPK)
	outputFolder_HPK += thickness + "um/"
	createFolder(outputFolder_HPK)
	outputFolder_HPK += nameSensor + "/"
	createFolder(outputFolder_HPK)

	#Define input file for HPK measurement
	fileName_HPK = "Hamamatsu_" + inches + "in_" + nCells + "ch_" + thickness + "um_" + typeSensor + "_" + nameSensor + "_IV"
	label_sensor_HPK = "\"Sensor: " + inches + "in, " + nCells + "ch, " + nameSensor + ", HPK\""
	fileNameList_HPK += inputFolder_HPK + fileName_HPK + ".txt,"

	if(analyse_CERN):
		print('IV analysis for ' + inputFolder_CERN + fileName_CERN)
		print('All results can be found in ' + outputFolder_CERN)
	if(analyse_HPK):
		print('IV analysis for ' + inputFolder_HPK + fileName_HPK)
		print('All results can be found in ' + outputFolder_HPK)

	#Plot maps for non-log scale
	z_min = "0"
	z_max = "100"

	if(analyse_CERN):
		if(analyse_IVmaps):
			plotIVmaps(inputFolder_CERN, fileName_CERN, outputFolder_CERN, z_min, z_max, False, label_sensor_CERN)
		if(analyse_IVallCells):
			plotIVallCells(inputFolder_CERN, fileName_CERN, outputFolder_CERN, z_min, z_max, False)

	if(analyse_HPK):
		if(analyse_IVmaps):
			plotIVmaps(inputFolder_HPK, fileName_HPK, outputFolder_HPK, z_min, z_max, False, label_sensor_HPK)
		if(analyse_IVallCells):
			plotIVallCells(inputFolder_HPK, fileName_HPK, outputFolder_HPK, z_min, z_max, False)

	#Plot ratio between HPK and CERN
	z_min = "-3"
	z_max = "+3"
	label_sensor_ratio = "\"Sensor: " + inches + "in, " + nCells + "ch, " + nameSensor + ", HPK/CERN\""

	if(analyse_IVmapsRatios):
		plotIVmapsRatio( inputFolder_HPK, fileName_HPK, inputFolder_CERN, fileName_CERN, outputFolder_HPK, z_min, z_max, False, label_sensor_ratio)
	
	#Plot maps for log scale
	z_min = "0.01"
	z_max = "100000"
	outputFolder_CERN += "logScale/"
	createFolder(outputFolder_CERN)
	outputFolder_HPK += "logScale/"
	createFolder(outputFolder_HPK)

	if(analyse_CERN):
		if(analyse_IVmaps):
			plotIVmaps(inputFolder_CERN, fileName_CERN, outputFolder_CERN, z_min, z_max, True, label_sensor_CERN)
		if(analyse_IVallCells):
			plotIVallCells(inputFolder_CERN, fileName_CERN, outputFolder_CERN, z_min, z_max, True)

	if(analyse_HPK):
		if(analyse_IVmaps):
			plotIVmaps(inputFolder_HPK, fileName_HPK, outputFolder_HPK, z_min, z_max, True, label_sensor_HPK)
		if(analyse_IVallCells):
			plotIVallCells(inputFolder_HPK, fileName_HPK, outputFolder_HPK, z_min, z_max, True)

	#Plot total current for CERN measurement	
	# z_max = "1000000"
	if(analyse_TotalIV and analyse_CERN):
		plotTotalCurrent(inputFolder_CERN, fileName_CERN, outputFolder_CERN, z_min, z_max, True, label_sensor_CERN)
	
print fileNameList_CERN
print fileNameList_HPK

outputFolder_CERN = "Hamamatsu_" + inches + "in_" + nCells + "ch_" + nameBatch + "_CERN/" + thickness + "um/"
outputFolder_HPK  = "Hamamatsu_" + inches + "in_" + nCells + "ch_" + nameBatch + "_HPK/" + thickness + "um/"

z_min = "0"
z_max = "100"

if(analyse_IVSensors and analyse_CERN):
	plotIVSensors(fileNameList_CERN, outputFolder_CERN, z_min, z_max, False)
if(analyse_IVSensors and analyse_HPK):
	plotIVSensors(fileNameList_HPK, outputFolder_HPK, z_min, z_max, False)

z_min = "100"
z_max = "30000"

if(analyse_IVSensors and analyse_CERN):
	plotIVSensorsTotCurr(fileNameList_CERN, outputFolder_CERN, z_min, z_max, False)

z_min = "0.01"
z_max = "100000"

if(analyse_IVSensors and analyse_CERN):
	plotIVSensors(fileNameList_CERN, outputFolder_CERN, z_min, z_max, True)
if(analyse_IVSensors and analyse_HPK):
	plotIVSensors(fileNameList_HPK, outputFolder_HPK, z_min, z_max, True)

if(analyse_IVSensors and analyse_CERN):
	plotIVSensorsTotCurr(fileNameList_CERN, outputFolder_CERN, z_min, z_max, True)

