#include "../src/utils/cpp_utils.cxx"
#include "../src/utils/root_utils.cxx"

void test_hash(uint64_t length = 100000) {
  printf("Testing my hash function\n");
  printf("Max hash size: %lu\n", MAX_HASH_SIZE);

  printf("----- Size test: -----\n");
  uint64_t testVals[] = {
      0,
      1,
      2,
      3,
      4,
      5,
      uint64_t(1e1),
      uint64_t(1e2),
      uint64_t(1e3),
      uint64_t(1e4),
      uint64_t(1e5),
      uint64_t(1e6),
      uint64_t(1e7),
      uint64_t(1e8),
      uint64_t(1e9),
      uint64_t(1e10),
      uint64_t(1e11),
      uint64_t(1e12),
      uint64_t(1e13),
      uint64_t(1e14),
      uint64_t(1e15),
      uint64_t(1e16),
      uint64_t(1e17),
      uint64_t(1e18),
      uint64_t(1e19),
      uint64_t(1e20),
      uint64_t(1e21),
      uint64_t(1e22),
  };
  for (auto& testVal : testVals) {
    std::cout << std::setw(21) << testVal << " : " << modulo_hash(testVal) << std::endl;
  }

  printf("----- Combination test: -----\n");
  uint64_t testHash = modulo_hash(13371337);
  for (auto& testVal : testVals) {
    uint64_t thisHash = modulo_hash(testVal);
    std::cout << testHash << " (+) " << std::left << std::setw(21) << thisHash << " : "
              << modulo_merge_hash(testHash, thisHash) << std::endl;
  }

  printf("----- Collision test: -----\n");
  auto* h_hash = new TH1F("h_hash", "h_hash", 1000, 0, length);
  uint64_t minHash = modulo_hash(0);
  uint64_t maxHash = modulo_hash(0);
  std::vector<uint64_t> hashes;
  for (uint64_t i = 0; i < length; ++i) {
    hashes.push_back(modulo_hash(i));
    h_hash->Fill(hashes.back());
  }
  std::sort(hashes.begin(), hashes.end());
  bool foundCollision = false;
  for (int i = 0; i < hashes.size() - 1; i++) {
    if (hashes[i] == hashes[i + 1]) {
      printf("Collision! Hashes %lu = %lu are equal!\n", hashes[i], hashes[i + 1]);
      foundCollision = true;
      break;
    }
  }
  if (!foundCollision) printf("Collision free for %lu tries\n", length);
  h_hash->Draw();
}
