/**
        @file hex_plotter.h
        @brief Plots values according to a given geometry in several gemoetrical
        shapes
        @author Andreas Alexander Maier
*/

#ifndef HEX_PLOTTER_H
#define HEX_PLOTTER_H

#include <limits>
#include <string>

#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1F.h"
#include "TLegend.h"
#include "TRandom.h"
#include "TTree.h"

#include "../geo_plot/geo_plot.h"
#include "../utils/cpp_utils.h"
#include "../utils/root_utils.h"
#include "../version.h"
#include "example_handler.h"
#include "flat_plot.h"
#include "globals.h"
#include "help_handler.h"
#include "hex_values.h"

/// The main plotting class
class hex_plotter {
 public:
  /// Parses the input parameters
  int parse_input();
  /// Parses the input parameters
  int parse_input(std::vector<std::string> argv_vec);
  /// Executes the plotting functions
  int run();
  /// Performs checks on the input parameters, e.g. file existence
  int check_input_parameters();
  /// Prints the help
  int print_help();
  /// Prints the list of examples
  int print_examples();
  /// Prints the version number
  int print_version();
  /// Opens a browser window to the documentation
  int show_doxygen();
  /// Prints the arguments after parsing the input parameters
  int print_defaults();
  /// Returns verbosity setting
  int get_verbose() { return verbose; };
  /// Constructor
  hex_plotter(int argC, char **argV);

 private:
  const int GEO_ONLY_COLOR = 38;

  // parameters without setter

  // setters that cannot overwrite standard parameters
  bool abortExecution;
  bool detailedPlots;
  bool invertX;
  bool invertVal;
  bool isCVInput;
  bool isIVInput;
  bool noAxis;
  bool noInfo;
  bool noLog;
  bool useLogVal;
  bool noTrafo;
  bool onlySpecial;
  bool openDoxygen;
  bool printDefaults;
  bool printExamples;
  bool printVersion;
  bool printHelp;
  bool showMaxValue;
  bool summaryFile;
  bool testInfo;
  bool yesToAll;
  int appearance;
  int executeExample;
  int overwritePadColors;
  int padNumberOption;
  int paletteNumber;
  int valuePrecision;
  int verbose;
  int mlw;
  int textColor;
  std::string fitFunc;
  std::string geoFile;
  std::string infoString;
  std::string inputFile;
  std::string mapFile;
  std::string mergeFileList;
  std::string outputFormat;
  std::string plotOption;
  std::string setFitFunc;
  std::string specialCellNameString;
  std::string specialCellString;
  std::string zoom;
  std::string zoomUnc;

  // setters that can overwrite standard parameters
  bool isAverageValues;
  bool setNoAverageValues;
  bool isRatioValues;
  double selector;
  double setSelector;
  double setYScale;
  double yScale;
  std::string aspectRatio;
  std::string additionalLabel;
  std::string baseValueName;
  std::string inputFormat;
  std::string outputFile;
  std::string selectorName;
  std::string setAspectRatio;
  std::string setInputFormat;
  std::string setOutputDir;
  std::string setOutputFile;
  std::string setSelectorName;
  std::string setSpecialCellOption;
  std::string setSummaryFileName;
  std::string setValueName;
  std::string specialCellOption;
  std::string summaryFileName;
  std::string valueName;

  // variable parameters
  bool isInitialized;
  bool isInterPadPlot;
  char **argv;
  double aspectRatioX;
  double aspectRatioY;
  double zoomMax;
  double zoomMin;
  double zoomUncMax;
  double zoomUncMin;
  int argc;
  std::map<std::string, std::string> infoMap;
  std::ofstream fileout;
  std::string valueOption;
  std::vector<std::string> baseValueLabels;
  std::vector<std::string> inputFiles;
  std::vector<std::string> mergeFiles;
  std::vector<std::string> selectorLabels;
  std::vector<std::string> specialCellCollection;
  std::vector<std::string> specialCellCollectionRange;
  std::vector<std::string> specialCellNames;
  std::vector<std::string> valueLabels;
  std::vector<std::vector<int>> specialCellNumbers;
  std::vector<std::vector<std::string>> specialCells;

  // custom class instances
  example_handler *exampleHandler;
  flat_plot *flatPlot;
  geo_plot *geoPlot;
  help_handler *helpHandler;
  hex_values *hexValues;

  /// Executes the specified example
  int execute_example();
  void init();
  void merge();
  void plot_geo();
  void plot_flat();
  void plot_pad();
  void plot_full();
  void print_parameters();
  void set_info_text();
};

#endif
