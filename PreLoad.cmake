# On Windows, force usage of Visual Studio 12 2013:
IF(WIN32)
    SET(CMAKE_GENERATOR "Visual Studio 12 2013" CACHE INTERNAL "" FORCE)
ENDIF()
